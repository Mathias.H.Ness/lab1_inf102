package INF102.lab1.triplicate;

import java.util.List;
import java.util.Collections;

public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
       
        Collections.sort(list);

        for (int i = 2; i < list.size(); i++) {
            T elem = list.get(i);
            if (elem.equals(list.get(i-1)) 
                && elem.equals(list.get(i-2))) return elem;
        }

        return null;
    }
    
}
